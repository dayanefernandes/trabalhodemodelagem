
package br.sistema.edu;

import java.util.ArrayList;
import java.util.List;
import java.util.Date ;

public class Compra {

	private int idCompra;
	private String itemCompra;
	private Date dtCompra;
	
	
	private List<ItemCompra> itens = new ArrayList<ItemCompra>();
	
		public Compra(int idCompra) {
			this.idCompra = idCompra;
		}
	
		public void addItemCompra(ItemCompra item) {
			itens.add(item);
		}
		
		public void removeItemCoompraPorDescricao(String descricao) {
			for(ItemCompra item: itens) {
				if(item.getDescricao().equals(descricao)) {
					itens.remove(item);
					break;
				}
			}
		}
	    private int getIdCompra() {
			return idCompra;
		}

		private void setIdCompra(int idCompra) {
			this.idCompra = idCompra;
		}

		private String getItemCompra() {
			return itemCompra;
		}
		
		private void setItemCompra(String itemCompra) {
			this.itemCompra = itemCompra;
		}

		private Date getDtCompra() {
			return dtCompra;
		}

		private void setDtCompra(Date dtCompra) {
			this.dtCompra = dtCompra;
		}
		
		public float totalizarItens() {
			float total= 0;
			for(ItemCompra item: itens) {
				total= total + item.getQuantidadeItem()*item.getValor();
			}
			return total;
		}
		
		private float getValor() {
			// TODO Auto-generated method stub
			return 0;
		}

		public List<ItemCompra> getItens(){
			return itens;
		
	}

		public int getIdProduto() {
			// TODO Auto-generated method stub
			return 0;
		}

		}



