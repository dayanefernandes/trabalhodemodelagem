package br.sistema.edu;

public class Produto {
	
	
	
	private  int  idProduto ;
	public  String  descricao ;
	private  int  quantidade ;
	private float preco ;
	private String dtcadastro;
	
	public Produto() {}
	
	
	public Produto(int idProduto, String descricao, float preco, int quantidade, String dtCadastro) {
		
		this.idProduto= idProduto;
		this.descricao= descricao;
		this.quantidade= quantidade;
		this.preco= preco;
		this.dtcadastro= dtCadastro;
		
	}
	

	
	private int getIdProduto() {
		return idProduto;
	}



	private void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}



	public String getDescricao() {
		return descricao;
	}



	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}



	private int getQuantidade() {
		return quantidade;
	}



	private void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}



	public float getPreco() {
		return preco;
	}



	private void setPreco(float preco) {
		this.preco = preco;
	}



	private String getDtcadastro() {
		return dtcadastro;
	}



	private void setDtcadastro(String dtcadastro) {
		this.dtcadastro = dtcadastro;
	}



	public void baixarProduto(int quantBaixada) {
		if (quantBaixada<= this.quantidade) {
			this.quantidade= this.quantidade- quantBaixada;
			
		}
		

	}

}
