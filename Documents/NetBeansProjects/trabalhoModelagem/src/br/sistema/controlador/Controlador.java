package br.sistema.controlador;

import java.util.ArrayList;
import java.util.List;

import br.sistema.edu.Cliente;
import br.sistema.edu.Compra;
import br.sistema.edu.ItemCompra;
import br.sistema.edu.Produto;
import br.sistema.edu.TipoEndereco;

public class Controlador {
	
private static Controlador sdv=null;
	
	
	private Cliente clienteAtual=null;
	
	private List<Produto> cadastroProduto = new ArrayList<Produto>();
	
	
	private List<Cliente> cadastroCliente = new ArrayList<Cliente>();
	
	private Controlador(){}

	public static Controlador getInstance(){
		if (sdv == null){
			sdv = new Controlador();
			return sdv;
		} else
			throw new RuntimeException("Controlador ja foi criado!!");

}

	public void cadastrarProduto(int idProduto,String descricao,float preco,int quantidade,TipoEndereco tipo){
			Produto prod = new Produto();
			boolean achou = false;
		for (Produto produto: cadastroProduto){
			if (produto.getDescricao().equals(descricao)){
				
				achou=true;
				break;
			}
			
			
		}
		if (! achou){
			cadastroProduto.add(prod);
		}
	}
	
	public List<String> listarProdutosCadastrados(){
		List<String> produtos = new ArrayList<String>();
		for (Produto produto: cadastroProduto){
			produtos.add(produto.getDescricao());
		}
		return produtos;
	}


	public void cadastrarCliente(String nome){
		Cliente clie = new cliente ();
		boolean achou = false;
		for (Cliente cliente: cadastroCliente){
			if (cliente.getNome().equals(nome)){
				achou=true;
				break;
			}
		}
		if (! achou ){
			cadastroCliente.add(clie);
		}
	}
		
	public List<String> listarClientesCadastrados(){
		List<String> clientes = new ArrayList<String>();
		for (Cliente cliente: cadastroCliente){
			clientes.add(cliente.getNome());
		}
		return clientes;
	}
	
	public void selecionarClientePorNome(String nome){
		for (Cliente clie: cadastroCliente){
			if (clie.getNome().equals(nome)){
				clienteAtual=clie;
			}
		}
	}
	
	
	public void abrirCompra(int numero){
		Compra compra = new Compra(numero);
		if (clienteAtual != null){
			clienteAtual.adicionarCompra(compra);
		} else
		   throw new RuntimeException("Cliente nÃ£o foi selecionado!!");
	}
	
	public void venderProduto(int idCompra,String descricaoProduto,int quantidade){
		if (clienteAtual != null){
			Compra compraAtual = clienteAtual.pegarCompraPorIdProduto(idCompra);
			if (compraAtual != null){
				for (Produto produto: cadastroProduto) {
					if (produto.getDescricao().equals(descricaoProduto)) {
						compraAtual.addItemCompra(new ItemCompra( descricaoProduto,quantidade,produto.getPreco()));			
						produto.baixarProduto(quantidade);
					} 
				}
			} else
			 throw new RuntimeException("Compra nÃ£o foi aberta!!");				
		} else
			throw new RuntimeException("Cliente não foi selecionado!!");
	}
	
	public float totalizarVenda(int idProduto) {
		float total=0;
		if (clienteAtual != null) {
			Compra compraAtual = clienteAtual.pegarCompraPorIdProduto(idProduto);
			if (compraAtual != null){
				total = compraAtual.totalizarItens();
			} else
			 throw new RuntimeException("Compra não foi aberta!!");				
		} else
			throw new RuntimeException("Cliente não foi selecionado!!");
		return total;
	}
	
}

